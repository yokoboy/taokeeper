package common.toolkit.java.exception;

/**
 * Description: Exception of illegal param.
 *
 * @author 银时 yinshi.nc@taobao.com
 */
public class IllegalParamException extends Exception {

    private static final long serialVersionUID = -5365630128856068164L;

    public IllegalParamException() {
        super();
    }

    public IllegalParamException(String message) {
        super(message);
    }

    public IllegalParamException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalParamException(Throwable cause) {
        super(cause);
    }

}

