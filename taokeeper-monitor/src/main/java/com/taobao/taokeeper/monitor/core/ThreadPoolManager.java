package com.taobao.taokeeper.monitor.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Description: 线程池管理
 *
 * @author 银时 yinshi.nc@taobao.com
 * @Date Dec 25, 2011
 */
public class ThreadPoolManager {

    private static Logger LOG = LoggerFactory.getLogger(ThreadPoolManager.class);

    private static int SIZE_OF_ZK_NODE_ALIVE_CHECK_EXECUTOR = 5; // 节点自检 线程池
    private static int SIZE_OF_MESSAGES_SEND_EXECUTOR = 5; // 消息发送 线程池
    private static int SIZE_OF_ZK_SERVER_STATUS_COLLECTOR_EXECUTOR = 3; // 收集ZKServer状态信息 线程池
    private static int SIZE_OF_ZK_SERVER_PERFORMAN_CECOLLECTOR_EXECUTOR = 3;// 收集ZKServer机器信息 线程池
    private static int SIZE_OF_ZK_CLUSTER_CONFIG_DUMPER_EXECUTOR = 2;


    public static void init() {
        if (null == zooKeeperNodeAliveCheckExecutor) {
            LOG.info("Start init ThreadPoolManager...");
            zooKeeperNodeAliveCheckExecutor = Executors.newFixedThreadPool(SIZE_OF_ZK_NODE_ALIVE_CHECK_EXECUTOR);
            messageSendExecutor = Executors.newFixedThreadPool(SIZE_OF_MESSAGES_SEND_EXECUTOR);
            zkServerStatusCollectorExecutor = Executors.newFixedThreadPool(SIZE_OF_ZK_SERVER_STATUS_COLLECTOR_EXECUTOR);
            zkServerPerformanceCollectorExecutor = Executors.newFixedThreadPool(SIZE_OF_ZK_SERVER_PERFORMAN_CECOLLECTOR_EXECUTOR);
            zkClusterConfigDumperExecutor = Executors.newFixedThreadPool(SIZE_OF_ZK_CLUSTER_CONFIG_DUMPER_EXECUTOR);
        }
    }


    /**
     * 节点自检 线程池
     */
    private static ExecutorService zooKeeperNodeAliveCheckExecutor;

    public static void addJobToZooKeeperNodeAliveCheckExecutor(Runnable command) {
        init();
        zooKeeperNodeAliveCheckExecutor.execute(command);
    }

    /**
     * 消息发送 线程池
     */
    private static ExecutorService messageSendExecutor;

    public static void addJobToMessageSendExecutor(Runnable command) {
        init();
        messageSendExecutor.execute(command);
    }

    /**
     * 收集ZKServer状态信息 线程池
     */
    private static ExecutorService zkServerStatusCollectorExecutor;

    public static void addJobToZKServerStatusCollectorExecutor(Runnable command) {
        init();
        zkServerStatusCollectorExecutor.execute(command);
    }


    /**
     * 收集ZKServer机器信息 线程池
     */
    private static ExecutorService zkServerPerformanceCollectorExecutor;

    public static void addJobToZKServerPerformanceCollectorExecutor(Runnable command) {
        init();
        zkServerPerformanceCollectorExecutor.execute(command);
    }

    /**
     * Dump zk cluster config info to memeory
     */
    private static ExecutorService zkClusterConfigDumperExecutor;

    public static void addJobToZKClusterDumperExecutor(Runnable command) {
        init();
        zkClusterConfigDumperExecutor.execute(command);
    }


}
