package common.toolkit.java.constant;


/**
 * Description:	Base Constant
 *
 * @author nileader / nileader@gmail.com
 * @Date Feb 15, 2012
 */
public class BaseConstant {

    /**
     * 一秒钟的毫秒数
     */
    public static long MILLISECONDS_OF_ONE_SECOND = 1000;

    /**
     * 一分钟的毫秒数
     */
    public static long MILLISECONDS_OF_ONE_MINUTE = 1000 * 60;

    /**
     * 一天的毫秒数
     */
    public static long MILLISECONDS_OF_ONE_DAY = 1000 * 60 * 60 * 24;

    /**
     * 一小时的毫秒数
     */
    public static long MILLISECONDS_OF_ONE_HOUR = 1000 * 60 * 60;

    /**
     * WORD_SEPARATOR  ( char )2
     */
    public static final String WORD_SEPARATOR = Character.toString((char) 2);

    /**
     * 配置文件全路路径 使用启动参数 -DconfigFilePath 传递
     */
    public static final String SYSTEM_PROPERTY_CONFIG_FILE_PATH = "configFilePath";

    /**
     * 配置文件名，从类路径下加载 -DconfigFileName 传递
     */
    public static final String SYSTEM_PROPERTY_CONFIG_FILE_NAME = "configFileName";

}
